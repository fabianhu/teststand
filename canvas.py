#!/usr/bin/env python3

from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib.pagesizes import A4, portrait
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Frame, Paragraph, Spacer
from reportlab.platypus.tables import Table
from reportlab.platypus import Image
import os.path

import random

from reportlab.lib import colors
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.widgets.markers import makeMarker
from statistics import median, mean
import scipy.signal


def save_json(file_path, dat):
    # this saves the array in .json format
    b = dat.tolist()  # nested lists with same data, indices
    json.dump(b, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


def load_json(file_path):
    obj_text = codecs.open(file_path, 'r', encoding='utf-8').read()
    b_new = json.loads(obj_text)
    return np.array(b_new)


def create_data(n):
    data = []
    # ((1,1), (2,2), (2.5,1.5), (3,3), (4,5)),((1,2), (2,3), (2.5,2), (3.5,5), (4,6))]

    for r in range(n):
        d2 = []
        n = 5
        for i in range(100):
            n = n * 0.7 + round(random.uniform(0.0, 10.0) * 0.3, 2)
            d2.append((i * 1, n))
            # d2.sort()

        data.append(d2)
    return data


def flt(sig, hop):
    res = []
    hop = 5
    for cnt in range(0, len(sig) - 1, hop):
        r = sig[cnt:cnt + hop]
        x, y = zip(*r)
        res.append((max(x), median(y)))
    # for x,y in sig:
    # s2 = scipy.signal.medfilt(sig, [3, 1])
    # return scipy.signal.resample_poly(s2, 1, 4, 0)
    return res


def create_pdf(name, data, data2, data3):
    drawing = Drawing(500, 300)

    lp = LinePlot()
    lp.x = 20
    lp.y = 0
    lp.height = 300
    lp.width = 500
    lp.data = data
    lp.joinedLines = 1
    # lp.lines[0].symbol = makeMarker('FilledCircle')
    # lp.lines[1].symbol = makeMarker('Circle')
    lp.lines[0].strokeColor = colors.red
    lp.lines[1].strokeColor = colors.green
    lp.lines[2].strokeColor = colors.blue
    lp.lineLabelFormat = '%2.1f'
    lp.lineLabels.fontSize = 8
    lp.lineLabels.fontName = "Helvetica"
    lp.xValueAxis.labels.fontName = "Helvetica"
    lp.yValueAxis.labels.fontName = "Helvetica"
    lp.strokeColor = colors.black
    lp.xValueAxis.valueMin = 0
    lp.xValueAxis.valueMax = 100
    lp.xValueAxis.visibleGrid = 1
    lp.xValueAxis.valueStep = 10
    lp.xValueAxis.labelTextFormat = '%2.1f'
    lp.yValueAxis.valueMin = 0
    lp.yValueAxis.valueMax = 10
    lp.yValueAxis.visibleGrid = 1
    lp.yValueAxis.valueStep = 1

    drawing.add(lp)

    # data = [data_Volt, data_Amp, data_GPW]
    # data2 = [data_Thrust, data_Watt]
    # data3 = [flt(data_GPW_over_g, 3), flt(data_Watt_over_g, 3)]

    startvolt = data[0][0][1]
    stopvolt = data[0][-1][1]
    maxgpw = max(data[2], key=lambda x: x[1])
    maxA = max(data[1], key=lambda x: x[1])
    maxW = max(data2[1], key=lambda x: x[1])
    maxg = max(data2[0], key=lambda x: x[1])


    drawing2 = Drawing(500, 150)

    lp2 = LinePlot()
    lp2.x = 20
    lp2.y = 0
    lp2.height = 150
    lp2.width = 500
    lp2.data = data3
    lp2.joinedLines = 1
    lp2.lines[0].strokeColor = colors.red
    lp2.lines[1].strokeColor = colors.green
    #lp2.lines[2].strokeColor = colors.blue
    # lp2.lines[0].symbol = makeMarker('FilledCircle')
    # lp2.lines[1].symbol = makeMarker('Circle')
    lp2.lineLabelFormat = '%2.1f'
    lp2.lineLabels.fontSize = 8
    lp2.lineLabels.fontName = "Helvetica"
    lp2.xValueAxis.labels.fontName = "Helvetica"
    lp2.yValueAxis.labels.fontName = "Helvetica"
    lp2.strokeColor = colors.black
    lp2.xValueAxis.valueMin = 0
    #lp2.xValueAxis.valueMax = round(maxg[1],0)+1
    lp2.xValueAxis.visibleGrid = 1
    lp2.xValueAxis.valueStep = 10
    lp2.xValueAxis.labelTextFormat = '%2.1f'
    lp2.yValueAxis.valueMin = 0
    # lp2.yValueAxis.valueMax = 10
    lp2.yValueAxis.visibleGrid = 1

    drawing2.add(lp2)



    tableItems = [
        ("Start voltage", str(round(startvolt, 1)), "V"),
        ("Stop voltage", str(round(stopvolt, 1)), "V"),
        ("Max Efficency", str(round(maxgpw[1], 1)) + " @ " + str(round(maxgpw[0], 0)) + "%", "g/W"),
        ("Max Thrust", str(round(maxg[1], 1)) + " @ " + str(round(maxg[0], 0)) + "%", "g"),
        ("Max Power", str(round(maxW[1], 1)) + " @ " + str(round(maxW[0], 0)) + "%", "W"),
        ("Max Current", str(round(maxA[1], 1)) + " @ " + str(round(maxA[0], 0)) + "%", "A")
    ]

    table = Table(data=tableItems, style=[
        ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
    ])

    # Anlegen des PDFs im DIN A4 Querformat
    pdf = canvas.Canvas("./measurements/" + name + '.pdf', pagesize=portrait(A4))
    # Holen der Seitenabmessung
    breite, hoehe = portrait(A4)
    # Definieren der Styles
    style = getSampleStyleSheet()

    story = []  # Anlegen einer Liste, welche den Seiteninhalt enthält

    f = Frame(10 * mm, 10 * mm, breite - (20 * mm),
              hoehe - (20 * mm))  # Anlegen des Frames, in den der Inhalt eingefügt wird
    # Generieren von Inhalt
    story.append(Paragraph("Propeller Test Report \"" + name + "\"", style['Heading2']))
    story.append(Paragraph('Generated by https://gitlab.com/fabianhu/teststand',
                           style['BodyText']))
    story.append(Spacer(100, 5 * mm))
    fname = "./measurements/" + name + ".png"

    if os.path.isfile(fname):
        im = Image(fname)
    else:
        im = Image('propeller.jpg')
    im.drawHeight = 48 * mm
    im.drawWidth = 64 * mm

    # table2 = Table(outerdata, colWidths=400)  # , rowHeights=50

    table2 = Table([[im, table]], style=[
        # ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'CENTER')
    ])

    story.append(table2)

    # data = [flt(data_Volt), flt(data_Amp), flt(data_GPW)]
    story.append(Paragraph('Voltage [V](red), Current [A](green), Efficiency [g/W](blue) over [%]', style['Heading2']))
    story.append(Spacer(100, 5 * mm))
    story.append(drawing)

    story.append(Spacer(100, 5 * mm))

    # data2 = [flt(data_Thrust), flt(data_Watt)]
    # data3 = [flt(data_GPW_over_g, 3), flt(data_Watt_over_g, 3)]
    story.append(Paragraph('Efficiency [g/W](red) , Current [A](green) over Thrust [g]', style['Heading2']))
    story.append(Spacer(100, 5 * mm))
    story.append(drawing2)

    # Hinzufügen des Inhalts
    f.addFromList(story, pdf)

    # ...und abspeichern
    pdf.setSubject("Test Report" + name)
    pdf.setTitle(name)
    pdf.setAuthor("Fabian Huslik")
    pdf.setCreator("We don't tell")
    pdf.setProducer("The Producer")
    pdf.save()

    print("PDF created.")


if __name__ == "__main__":
    """ This is executed when run from the command line """
    d = create_data(3)
    d2 = create_data(2)

    dx = flt(d2[0], 5)

    d3 = [d2[0], dx]

    create_pdf("test", d, d3)
