#!/usr/bin/python3

import json
from hx711 import HX711
import time

referenceUnit = 1840

# fire up the load cell
hx = HX711(5, 6)
hx.set_reading_format("MSB", "MSB")
hx.set_reference_unit(referenceUnit)  # fixme load !
hx.reset()
hx.tare()

for i in range(10):
    thrust_g = hx.get_weight(5)
    print(f"Measure {thrust_g}")

hx.set_reference_unit(1)
hx.reset()
hx.tare(30)

input("Put the 100g weight on scale and press any key.")
accu = 0

for i in range(20):
    thrust_g = hx.get_weight(5)
    accu += thrust_g
    print(f"{i} calibrating {thrust_g}")

accu /= 20

ref = accu / 100
print(f"Old reference is {referenceUnit}")
print(f"New reference is {ref}")
input("Remove weight and press any key.")

caldata = (ref, hx.OFFSET)  # gain,offset

with open("calibration.json", "w") as f:
    json.dump(caldata, f)
    #f.write(j)
    #f.close()

hx.set_reference_unit(ref)
hx.reset()
hx.tare()

print("Calibration finished, you can interrupt at any time now.")

time.sleep(5)

for i in range(1000):
    thrust_g = hx.get_weight(5)
    print(f"Measure {thrust_g}")
