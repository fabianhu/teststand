#!/usr/bin/python3

print("loading")
import codecs
import json
import os
import time

import numpy as np
import pigpio

import canvas
from ADS1x15 import ADS1015
from canvas import flt
from hx711 import HX711

# Parameters

MAXPOWER = 60
POWERSTEP = 0.5
HOLDATPWR = 110  # 110 is "not"
HOLDTIME = 60

AMPLIMIT = 5.5
VOLTLIMIT = 6.0
VOLTLIMIT_MAX = 12

last_time = time.time()

with open('calibration.json', 'r') as f:
    cal_gain, cal_offset = json.loads(f.read())

if cal_gain is None or cal_offset is None:
    print("calibrate first")
    exit(1)


def time_reset():
    global last_time
    last_time = time.time()


def time_get():
    global last_time
    return time.time() - last_time


print("starting")

measurement_name = input("Enter DUT configuration: ")

checkpath = "measurements/" + measurement_name + ".csv"


def get_nonexistant_path(fname_path):
    # Get the path to a filename which does not exist by incrementing path.
    if not os.path.exists(fname_path):
        return fname_path
    filename, file_extension = os.path.splitext(fname_path)
    _i = 1
    new_fname = "{}-{}{}".format(filename, _i, file_extension)
    while os.path.exists(new_fname):
        _i += 1
        new_fname = "{}-{}{}".format(filename, _i, file_extension)
    return new_fname


newpath = get_nonexistant_path(checkpath)
print(newpath)
newpath = newpath.split('/')[-1]
measurement_name = newpath.split('.')[0]
print(measurement_name)

output_csv = open("measurements/" + measurement_name + ".csv", "a")

output_csv.write(measurement_name + '\n')
output_csv.write("volt,amp,thrust_g,watt,power,g_per_W\n")

# fire up the ADC
adc = ADS1015()

# fire up the gauge
hx = HX711(5, 6)
hx.set_reading_format("MSB", "MSB")
hx.set_reference_unit(cal_gain)
# with open('calibration.json', 'r') as j:
#     data = json.load(j)
hx.reset()
hx.tare()
print(f"load cell calibration offset: {hx.OFFSET - cal_offset}")

# fire up the servo
PULSEMIN = 1000.0
PULSEMAX = 2000.0

pwm = pigpio.pi()
servopin = 17
pwm.set_mode(servopin, pigpio.OUTPUT)
pwm.set_PWM_frequency(servopin, 50)
pwm.set_PWM_range(servopin, 20000)  # us range
pwm.set_PWM_dutycycle(servopin, PULSEMIN)

print("start")

# take one photo of the prop using raspistill
from picamera import PiCamera
from time import sleep

print("Hold still, taking picture")
camera = PiCamera()
camera.start_preview()
sleep(3)
camera.capture("measurements/" + measurement_name + ".png")
camera.stop_preview()
print("picture done")

state = 0
amp_zero = 0
power = 0.0

data_Volt = []
data_Amp = []
data_Thrust = []
data_Watt = []
data_GPW = []
data_GPW_over_g = []
data_Watt_over_g = []
data_Amp_over_g = []

while True:
    thrust_g = hx.get_weight(5)  # number must be odd, as median value is taken.

    volt = adc.read_adc(1, gain=2) * 12.0 / 2000
    avolt = adc.read_adc(0, gain=2) * 5 / 2
    avolt -= 500  # offset of 0.5V
    amp = avolt / 133  # 133 mV /A

    if state == 0:  # wait for scale
        if abs(thrust_g) < 0.5:
            print("scale ready")
            time_reset()
            state = 1
        power = 0
    elif state == 1:  # wait for scale to be zero
        if abs(thrust_g) < 0.3:
            print("scale zero")
            time_reset()
            state = 2
        if time_get() > 10:  # wait for scale to get zero
            print("scale timeout",thrust_g,"g")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        power = 0
    elif state == 2:  # check volt & amp
        if 7.0 < volt < VOLTLIMIT_MAX and -0.1 < amp < 0.3:
            print("Voltage and current OK")
            time_reset()
            state = 3
            amp_zero = amp
        if time_get() > 10:  # wait for cuurent and amp to be OK
            print("volt/current timeout:",volt,"V",amp,"A")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        power = 0
    elif state == 3:  # wait before ramp up
        amp = amp - amp_zero  # apply correction
        if volt < 7.0 or volt > VOLTLIMIT_MAX:
            print("ERROR voltage",volt,"V")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if amp < -0.1 or amp > 0.5:
            print("ERROR idle current:",amp,"A")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if time_get() > 2:  # wait for ESC
            print("Starting ramp")
            state = 4
            time_reset()
    elif state == 4:  # ramp up
        amp = amp - amp_zero  # apply correction
        if volt < VOLTLIMIT or volt > VOLTLIMIT_MAX:
            print("ERROR voltage:",volt,"V")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if amp < -0.1 or amp > AMPLIMIT:
            print("ERROR current:",amp,"A")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if power == HOLDATPWR:
            print("holding")
            time_reset()
            state = 5
        elif power < MAXPOWER:
            power += POWERSTEP
        else:
            power = MAXPOWER
            print("ramping down")
            state = 6
    elif state == 5:  # wait HOLDTIME
        amp = amp - amp_zero  # apply correction
        if volt < VOLTLIMIT or volt > VOLTLIMIT_MAX:
            print("ERROR voltage",volt,"V")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if amp < -0.1 or amp > AMPLIMIT:
            print("ERROR current:",amp,"A")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if time_get() > HOLDTIME:  # wait for time expiry
            print("continue")
            power += POWERSTEP
            state = 4
    elif state == 6:  # ramp down
        amp = amp - amp_zero  # apply correction
        if volt < VOLTLIMIT or volt > VOLTLIMIT_MAX:
            print("ERROR voltage",volt,"V")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if amp < -0.1 or amp > AMPLIMIT:
            print("ERROR current:",amp,"A")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            exit(1)
            break
        if power > 0:
            power -= 5
        else:
            power = 0
            print("Run completed")
            pwm.set_PWM_dutycycle(servopin, PULSEMIN)
            break
    else:
        pwm.set_PWM_dutycycle(servopin, PULSEMIN)
        break

    pulsewidth = max(PULSEMIN, min(PULSEMAX, PULSEMIN + power / 100.0 * (PULSEMAX - PULSEMIN)))
    pwm.set_PWM_dutycycle(servopin, pulsewidth)

    if 4 <= state < 6:
        watt = volt * amp
        if watt > 0:
            gpw = thrust_g / watt
        else:
            gpw = 0
        s = f"{volt:.2f} V, {amp:.2f} A, {thrust_g:.2f} g, {watt:.2f} W, {power:.0f} %, {gpw:.2f} g/W"
        f = f"{volt:.2f}, {amp:.2f}, {thrust_g:.2f}, {watt:.2f}, {power:.0f}, {gpw:.2f}"
        output_csv.write(f + '\n')
        print(s)

        data_Volt.append((power, volt))
        data_Amp.append((power, amp))
        data_Thrust.append((power, thrust_g))
        data_Watt.append((power, watt))
        data_GPW.append((power, gpw))
        data_Amp_over_g.append((thrust_g, amp))
        data_GPW_over_g.append((thrust_g, gpw))
        data_Watt_over_g.append((thrust_g, watt))

output_csv.close()

print("measurement complete, creating PDF ...")

data = [flt(data_Volt, 5), flt(data_Amp, 5), flt(data_GPW, 5)]
data2 = [flt(data_Thrust, 7), flt(data_Watt, 7)]
data3 = [flt(data_GPW_over_g, 3), flt(data_Amp_over_g, 3)]

canvas.create_pdf(measurement_name, data, data2, data3)

exit(0)
