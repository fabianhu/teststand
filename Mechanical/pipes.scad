//$fn = 60;
wall = 1.2;

module flange(dia,wall){
    holedia = 2;
    difference(){
        union(){
            cylinder(d=dia+5*wall,h=wall);
            translate([0,0,wall])cylinder(d1=dia+5*wall, d2=dia+2*wall,h=wall*2);
            for(i=[45/2:45:360]){
                rotate([0,0,i])translate([dia/2+wall+holedia/2,0,0]) cylinder(d=2*wall+holedia,h=5);
            }
        }
        translate([0,0,-1])cylinder(d=dia,h=5+2);
        for(i=[45/2:45:360]){
            rotate([0,0,i])translate([dia/2+wall+holedia/2,0,0]) cylinder(d=holedia,h=6);
        }
    }

}

module lip(dia,rad,wall){
    intersection(){
        rotate_extrude(){
            difference(){
            translate([dia/2+rad,0,0]) circle(r=rad+wall);
            translate([dia/2+rad,0,0]) circle(r=rad);
            }
        }
        cylinder(d=dia+rad*1.7,h=rad);
    }
    //flange(dia,wall);
    translate([0,0,-20])
    difference(){
        cylinder(d=dia,h=20);
        cylinder(d=dia-2*wall,h=20);
    }
}

module rectifier(dia,wall,h){
    he=15;
    cylinder(d1=dia+wall*2,d2=dia,h=h);
    //flange(dia,wall);
    translate([0,0,-he])difference(){
        cylinder(d=dia+2*wall,h=he);
        cylinder(d=dia,h=he);
    }
}


module bow(out,rad,wall,angle){
    rotate_extrude(angle=angle){
        translate([rad,0,0])difference(){
            circle(d=out+wall*2);
            circle(d=out);
        }
    }
    rotate([-90,0,0]) translate([rad,0,0]) flange(out,wall);
    rotate([ 90,0,angle]) translate([rad,0,0]) flange(out,wall);
}
$fn=60;
module xxx(out,rad,wall,angle){
    difference(){
        rotate_extrude(angle=angle){
            translate([rad,0,0])difference(){
                circle(d=out);
            }
        }
        cylinder(r=rad*1.2,h=out,center=true);
    }
    intersection(){
        union(){translate([rad,0,0])rotate([-90,0,0])cylinder(d=out,h=rad);
        rotate([0,0,angle])translate([rad,0,0])rotate([90,0,0])cylinder(d=out,h=rad);
        }
        cylinder(r=rad*1.2,h=out,center=true);
    }
}

//xxx(104,52,wall,90);

module bowin(out,rad,wall,angle){
    w = 0.9;
    j = 6;
    k = 4;
    difference(){
        union(){
    intersection(){
        xxx(out,rad,wall,angle);
        union(){
            for(d = [rad-out/2-w:(out+2*w)/j:rad+out/2-w-1]){
                difference(){
                    cylinder(r=d+w/2,h=out,center=true);
                    cylinder(r=d-w/2,h=out,center=true);
                }
            }
                
            for(o = [-out/2-w:(out+2*w)/k:out/2-w-1]){
                        translate([0,0,o])
                        cylinder(d=out+2*rad,h=w,center=true);
            }
        }
        rotate([0,0,45]) translate([0,0,-150/2])cube(150);
    }
    rotate([0,0,angle/2])
            for(d = [rad-out/2-w:(out+2*w)/j*2:rad+out/2-w-1]){
                for(o = [-out/2-w:(out+2*w)/k:out/2-w-1]){
                        translate([d,0,o])
                        rotate([-90,0,0])cylinder(d1=3.5,d2=2,h=5);
                }
            }
            
        }
    rotate([0,0,angle/2])for(d = [rad-out/2-w:(out+2*w)/j*2:rad+out/2-w-1]){
                for(o = [-out/2-w:(out+2*w)/k:out/2-w-1]){
                        translate([d,0,o])
                        rotate([-90,0,0])cylinder(d=1.5,h=5);
                }
            }
        }
    
}

module bowinx(out,rad,wall,angle){
    w = 0.9;
    difference(){
        intersection(){
            xxx(out,rad,wall,angle);
            for(d = [rad-out/2:12:rad+out/2]){
                difference(){
                    cylinder(r=d+w/2,h=out,center=true);
                    cylinder(r=d-w/2,h=out,center=true);
                }
            }
        }
    rotate([-90,0,0]) translate([rad,0,0])cylinder(d=out,h=0.15);
    rotate([-90,0,angle]) translate([rad,0,0-0.15])cylinder(d=out,h=0.15);
    }    
}

module straight(out1,out2,wall,length){
    flange(out1,wall);
    difference(){
        cylinder(d1=out1+2*wall,d2=out2+2*wall,h=length);
        cylinder(d1=out1,d2=out2,h=length);
    }
    //translate([0,0,length]) rotate([180,0,0])flange(out2,wall);
}
/*difference(){
    union(){
        translate([0,0,0])rotate([0,0,0]) bowin(80,50,wall,180);
        translate([0,0,0])rotate([0,0,0]) bow(80,50,wall,180);
    }
    translate([-100,0,-200]) cube(200);
}
difference(){
    cylinder(d=2*50+80,h=0.15);
    translate([-100,-200,0]) cube(200);
}

*/

module stand(d,h,w){
    wi = d-w*2;
        dz = h-d/2;
    difference(){
        translate([-wi/2,-w/2,-h])cube([wi,w,h]);
        rotate([90,0,0]) cylinder(d=d,h=w,center=true);
        

        translate([dz-w,0,-h+dz/2+w/2])rotate([90,0,0]) cylinder(d=dz,h=w,center=true);
        translate([-dz+w,0,-h+dz/2+w/2])rotate([90,0,0]) cylinder(d=dz,h=w,center=true);
        translate([wi/2,0,-d/2+w*1.5])rotate([0,30,0])cylinder(d=4,h=50,center=true);
        translate([-wi/2,0,-d/2+w*1.5])rotate([0,-30,0])cylinder(d=4,h=50,center=true);
        ws = 2*(wi/2-w/2);
        translate([ws/2,0,-h]) cylinder(d=4,h=30);
        translate([-ws/2,0,-h]) cylinder(d=4,h=30);
        echo(ws);
        hull(){
        translate([0,0,-h+dz/2+w])rotate([90,0,0]) cylinder(d=dz*0.4,h=w,center=true);
        translate([0,0,-h+dz/2-w*1.3])rotate([90,0,0]) cylinder(d=dz*0.4,h=w,center=true);
        }
    }
    
    
}

stand(116,125,10);
translate([0,20,0])stand(110,125,10);

//translate([51,-10,0])rotate([90,0,0])straight(80,80,1,180);
translate([-150,-10,0])rotate([0,0,0])lip(110,30,2.5);

//translate([0,-180-20,0])rotate([0,0,180]) bow(80,50,1,180);

translate([-50,-180-10,0])rotate([-90,0,0]) rectifier(110,2,40);


bowin(104,54,wall,90);
//rotate([180,0,90])bowin(104,54,wall,90);