
module emstop(d){
    $fn=60;
    color("grey") cylinder(d=18,h=12.7);
    color("red") translate([0,0,12.7]) cylinder(d=23.2,h=4.5);
    
    color("darkgrey") translate([0,0,-10])intersection(){
        translate([0,0,5])cube([14.9,20,10],center=true);
        cylinder(d=16,h=10);
    }
    color("darkgrey") translate([0,0,-21]) cylinder(d=14.9,h=12);
    color("grey") translate([3,0,-21-7/2]) cube([2,13,7],center=true);
    
    color("grey") translate([0,0,-9-d]) cylinder(d=18.5,h=9);
}

module key(){
    $fn=60;
    intersection(){
        difference(){
            cylinder(d=19,h=28);
            cylinder(d=16,h=29);
        }
        union(){
        translate([0,0,2])cylinder(d=19,h=28);
        cube([5,20,4],center=true);
        cube([20,5,4],center=true);
        }
    }
}

module hodler(){
    difference(){
        hull(){
            translate([0,0,29]) cylinder(d=23,h=1);
            translate([0,0,0.5])cube([30,30,1],center=true);
            
        }
        cylinder(d=20,h=28);
        translate([0,0,30]) emstop(2);    
        rotate([90,0,0]) cylinder(d=5,h=30);
    }
    //translate([0,0,30]) emstop(2);
}

emstop(2);
translate([30,0,0])key();
translate([-30,0,0])hodler();